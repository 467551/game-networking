using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitGame : NetworkBehaviour
{
    public override void OnStartClient()
    {
        gameObject.SetActive(false);
    }

    public void ClientQuiteGame()
    {
        NetworkManager.singleton.StopClient();
        SceneManager.LoadScene("MainMenu");
    }
}
