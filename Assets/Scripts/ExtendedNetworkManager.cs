using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ExtendedNetworkManager : NetworkManager
{
    public override void OnStartServer()
    {
        Application.targetFrameRate = singleton.serverTickRate;
    }
}
