using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlayerTrigger : MonoBehaviour
{
    public delegate void PlayerTriggerEvent();
    public event PlayerTriggerEvent OnPlayerEnter;
    public event PlayerTriggerEvent OnPlayerExit;

    public bool _playerIsInTrigger = false;
    public bool isPlayerInTrigger {get => _playerIsInTrigger;}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerIsInTrigger = true;
            OnPlayerEnter?.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerIsInTrigger = false;
            OnPlayerEnter?.Invoke();
        }
    }
}
