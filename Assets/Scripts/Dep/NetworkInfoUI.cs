using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Player;
using UnityEngine;
using TMPro;

public class NetworkInfoUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _serverDeltaValueText;
    [SerializeField] private TextMeshProUGUI _serverDeltaTargetText;
    [SerializeField] private TextMeshProUGUI _rttText;

    private PlayerPacketTimes _netInfo;

    public void Init(PlayerPacketTimes pNetInfo)
    {
        _netInfo = pNetInfo;
        StartCoroutine(UpdateNetworkInfo());
    }

    private IEnumerator UpdateNetworkInfo()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            _serverDeltaTargetText.text = NetworkManager.singleton.serverTickRate.ToString();
            _serverDeltaValueText.text = string.Format("{0:N4}", (_netInfo.AverageServerDelta*100));
            _rttText.text = string.Format("{0:N4}", (_netInfo.AvarageRTT*100));
        }
    } 
}
