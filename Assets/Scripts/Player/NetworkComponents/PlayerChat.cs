using Cinemachine.Utility;
using Mirror;
using Network_Packets;
using Player.NetworkHandlers;
using UnityEngine;

namespace Player.NetworkComponents
{
    public class PlayerChat : NetworkComponent
    {
        public override void ClientStart()
        {
            if (isLocalPlayer)
            {
                parentView.ui.chatBox.OnSendChatMessage += OnSendRequest;
            }
        }
        
        public void HandlePacket(ChatMessagePacket packet)
        {
            if (isLocalPlayer)
            {
                parentView.SetChatMessage(packet.playerName,packet.chatMessage);
            }
            else
            {
                float distance =
                    (GameObject.FindGameObjectWithTag("LocalPlayer").transform.position - packet.positionMessenger).magnitude;

                if (distance < 45f)
                    parentView.SetChatMessage(packet.playerName,packet.chatMessage);
            }
        }
        
        public void OnSendRequest(string message)
        {
            ChatMessagePacket packet = new ChatMessagePacket()
                {positionMessenger = parentNetworkComponent.transform.position,playerName = PlayerPrefs.GetString("PlayerName"), chatMessage = message};
            SendToServer(packet);
        }

        // Transfer methods
        [Client]
        private void SendToServer(ChatMessagePacket packet)
        {
            CmdPlayerInfo(packet);
        }

        [Server]
        private void SendToClient(ChatMessagePacket packet)
        {
            RpcPlayerInfo(packet);
        }

        [Command(channel = Channels.Reliable)]
        private void CmdPlayerInfo(ChatMessagePacket packet)
        {
            packet.playerName = parentNetworkComponent.playerInfo.name;

            if (packet.chatMessage != string.Empty && packet.chatMessage.Length <= 64)
                SendToClient(packet);
        }

        [ClientRpc(channel = Channels.Reliable)]
        private void RpcPlayerInfo(ChatMessagePacket packet)
        {
            HandlePacket(packet);
        }
    }
}
