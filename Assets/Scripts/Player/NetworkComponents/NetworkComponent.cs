using Mirror;
using UnityEngine;

namespace Player.NetworkHandlers
{
    public abstract class NetworkComponent : NetworkBehaviour
    {
        protected PlayerNetworkComponent parentNetworkComponent;
        protected PlayerView parentView;
        protected PlayerInputHandler parentInput;
        
        public virtual void ClientStart(){}
        public virtual void ServerStart(){}
        
        public virtual void ServerTick(uint tick){}
        public virtual void ClientTick(uint tick){}

        public void SetComponentParent(PlayerNetworkComponent pNetworkComponent)
        {
            if (pNetworkComponent != null)
                parentNetworkComponent = pNetworkComponent;
        }

        public void SetView(PlayerView pView)
        {
            if (pView != null)
                parentView = pView;
        }

        public void SetInputHandler(PlayerInputHandler pInputHandler)
        {
            if (pInputHandler != null)
                parentInput = pInputHandler;
        }
    }
}
