using Mirror;
using Network_Packets;
using Player.NetworkHandlers;
using UnityEngine;

namespace Player
{
    public class NetworkStatus : NetworkComponent
    {
        private PlayerPacketTimes _packetTimes;
        public PlayerPacketTimes packetTimes {get => _packetTimes;}
        
        private PacketInfo latestPacket;

        private bool isInit = false;
        
        // Client

        [Client]
        private void ClientHandlePacket(PacketInfo info)
        {
            if (isInit)
                _packetTimes.CalculateTimestamps(info);
            
        }
        
        public override void ClientStart()
        {
            _packetTimes = new PlayerPacketTimes();
            isInit = true;
        }

        public override void ClientTick(uint tick)
        {
            if (isLocalPlayer)
            {
                PacketInfo packet = new PacketInfo() {clientTick = tick, clientTimestamp = NetworkTime.localTime};
                SendToServer(packet);
            }
        }
        
        // Server
        public override void ServerStart()
        {
            _packetTimes = new PlayerPacketTimes();
        }

        public override void ServerTick(uint tick)
        {
            latestPacket.serverTick = tick;
            latestPacket.serverTimestamp = NetworkTime.localTime;

            SendToClient(latestPacket);
        }
        
        // Transfer methods
        [Client]
        private void SendToServer(PacketInfo packet)
        {
            CmdHeartbeat(packet);
        }

        [Server]
        private void SendToClient(PacketInfo packet)
        {
            RpcHeartbeat(packet);
        }

        [Command(channel = Channels.Reliable)]
        private void CmdHeartbeat(PacketInfo packet)
        {
            latestPacket = packet;
        }

        [ClientRpc(channel = Channels.Reliable)]
        private void RpcHeartbeat(PacketInfo packet)
        {
            ClientHandlePacket(packet);
        }
    }
}
