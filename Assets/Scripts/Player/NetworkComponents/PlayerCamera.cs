using System;
using Player.NetworkHandlers;
using Cinemachine;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Player.NetworkComponents
{
    public class PlayerCamera : NetworkComponent
    {
        [SerializeField] private GameObject freeLookPrefab;
        private CinemachineFreeLook freeLook;

        private float _xSpeed = 0;
        private float _ySpeed = 0;
        
        public override void ClientStart()
        {
            if (!isLocalPlayer)
                return;

            freeLook = Instantiate(freeLookPrefab, Vector3.zero, Quaternion.identity).GetComponent<CinemachineFreeLook>();
            var plTransform = parentView.transform;
            freeLook.LookAt = plTransform;
            freeLook.Follow = plTransform;
            _xSpeed = freeLook.m_XAxis.m_MaxSpeed;
            _ySpeed = freeLook.m_YAxis.m_MaxSpeed;

            freeLook.m_XAxis.m_MaxSpeed = 0;
            freeLook.m_YAxis.m_MaxSpeed = 0;
            
            parentInput.OnRightMousePressed += CameraMoveInputUpdate;
        }

        private void CameraMoveInputUpdate(InputAction.CallbackContext pCtx,Vector2 pMouseScreenPosition)
        {
            switch (pCtx.phase)
            {
                case InputActionPhase.Performed:
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        freeLook.m_XAxis.m_MaxSpeed = _xSpeed;
                        freeLook.m_YAxis.m_MaxSpeed = _ySpeed; 
                    }
                    break;
                case InputActionPhase.Canceled:
                    freeLook.m_XAxis.m_MaxSpeed = 0;
                    freeLook.m_YAxis.m_MaxSpeed = 0;
                    break;
                case InputActionPhase.Disabled:
                    freeLook.m_XAxis.m_MaxSpeed = 0;
                    freeLook.m_YAxis.m_MaxSpeed = 0;
                    break;
            }
        }
        
        private void Update()
        {
            if (!isLocalPlayer)
                return;
        }
    }
}
