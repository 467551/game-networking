using Mirror;
using Network_Packets;
using Player.NetworkHandlers;
using UnityEngine;

namespace Player.NetworkComponents
{
    public class PlayerStep : NetworkComponent
    {
        [SerializeField] private PathStepper pathStepper;
        [SerializeField] private MovePredictor predictor;
        
        private float stepSize = 0f;
        private StepPacket latestPacket;

        // Client

        [Client]
        private void ClientHandlePacket(StepPacket packet)
        {
            latestPacket = packet;
            if (!isLocalPlayer)
            {
                pathStepper.ManualPosition(packet.position);

                parentView.Move(packet.position,parentNetworkComponent.status.packetTimes.AverageServerDelta,parentNetworkComponent.variables.movementSpeed * parentNetworkComponent.status.packetTimes.StepDelta);
            }
        }
        
        public override void ClientStart()
        {
            stepSize = parentNetworkComponent.variables.movementSpeed / NetworkManager.singleton.serverTickRate;
        }

        public override void ClientTick(uint tick)
        {
            predictor.CheckPredictedMovement(latestPacket);
            
            Vector3 predictedStep = predictor.DoStep(stepSize,tick);
            pathStepper.ManualPosition(predictedStep);
            parentView.Move(predictedStep,1f / NetworkManager.singleton.serverTickRate,parentNetworkComponent.variables.movementSpeed);

            PacketInfo info = new PacketInfo() {clientTick = tick,clientTimestamp = NetworkTime.localTime};
            SendToServer(predictedStep,info);
        }
        
        // Server
        
        public override void ServerStart()
        {
            stepSize = parentNetworkComponent.variables.movementSpeed / NetworkManager.singleton.serverTickRate;
        }

        public override void ServerTick(uint tick)
        {
            latestPacket.info.serverTick = tick;
            latestPacket.info.serverTimestamp = NetworkTime.localTime;

            latestPacket.position = pathStepper.DoStep(stepSize);
            SendToClient(latestPacket);
        }
        
        
        // Transfer methods
        
        [Client]
        private void SendToServer(Vector3 pStep, PacketInfo pInfo)
        {
            StepPacket packet = new StepPacket(){position= pStep, info = pInfo };
            CmdStep(packet);
        }

        [Server]
        private void SendToClient(StepPacket packet)
        {
            RpcStep(packet);
        }

        [Command(channel = Channels.Reliable)]
        private void CmdStep(StepPacket packet)
        {
            latestPacket = packet;
        }

        [ClientRpc(channel = Channels.Reliable)]
        private void RpcStep(StepPacket packet)
        {
            ClientHandlePacket(packet);
        }
    }
}
