using Mirror;
using Network_Packets;
using Player.NetworkHandlers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Player.NetworkComponents
{
    [RequireComponent(typeof(PathStepper),typeof(MovePredictor))]
    public class PlayerMovement : NetworkComponent
    {
        private PathStepper _pathStepper;
        private MovePredictor _predictor;
        
        private float _stepSize = 0f;
        private StepPacket _latestStepPacket;
        private DestinationPacket _latestDestinationPacket;
        
        private bool _destinationRequest = false;
        private Vector3 _latestDestination;

        // Client
        public override void ClientStart()
        {
            _pathStepper = gameObject.GetComponent<PathStepper>();
            _predictor = gameObject.GetComponent<MovePredictor>();
            
            _stepSize = parentNetworkComponent.variables.movementSpeed / NetworkManager.singleton.serverTickRate;
                
            if (isLocalPlayer)
                parentInput.OnLeftMousePressed += UpdateDestination;
        }

        private void UpdateDestination(InputAction.CallbackContext pCtx,Vector2 pMouseScreenPosition)
        {
            switch (pCtx.phase)
            {
                case InputActionPhase.Performed:
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        if (Physics.Raycast(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()),out RaycastHit hit,float.MaxValue,1 << LayerMask.NameToLayer("ClickableTerrain") | (1 << LayerMask.NameToLayer("CityBuilding"))))
                        {
                            _destinationRequest = true;
                            _latestDestination = hit.point;   
                        }
                    }
                    break;
            }
        }
        
        [Client]
        private void ClientHandlePacket(StepPacket packet)
        {
            _latestStepPacket = packet;
            if (!isLocalPlayer)
            {
                _pathStepper.ManualPosition(packet.position);

                parentView.Move(packet.position,parentNetworkComponent.status.packetTimes.AverageServerDelta,parentNetworkComponent.variables.movementSpeed);
            }
        }
        
        [Client]
        private void ClientHandlePacket(DestinationPacket packet)
        {
            if (isLocalPlayer)
            {
                _destinationRequest = true;
                _latestDestinationPacket = packet;
            }
        }

        public override void ClientTick(uint tick)
        {
            DestinationPacket destPacket = new DestinationPacket();
            if (_destinationRequest)
            {
                _pathStepper.SetDestination(_latestDestination);
                PacketInfo dInfo = new PacketInfo() {clientTick = tick,clientTimestamp = NetworkTime.localTime};
                destPacket = new DestinationPacket() {info = dInfo, destination = _latestDestination};
            }

            if (_latestStepPacket.info.serverTick != 0)
                _predictor.CheckPredictedMovement(_latestStepPacket);

            
            Vector3 predictedStep = _predictor.DoStep(_stepSize,tick);
            parentView.Move(predictedStep,1f / NetworkManager.singleton.serverTickRate,parentNetworkComponent.variables.movementSpeed);

            PacketInfo sInfo = new PacketInfo() {clientTick = tick,clientTimestamp = NetworkTime.localTime};
            StepPacket stepPacket = new StepPacket() {info = sInfo, position = predictedStep};

            if (_destinationRequest)
                SendToServer(destPacket);
            
            SendToServer(stepPacket);
        }
        
        // Server
        public override void ServerStart()
        {
            _pathStepper = gameObject.GetComponent<PathStepper>();
            _stepSize = parentNetworkComponent.variables.movementSpeed / NetworkManager.singleton.serverTickRate;
        }

        public override void ServerTick(uint tick)
        {
            if (_destinationRequest)
            {
                _pathStepper.SetDestination(_latestDestinationPacket.destination);
                _latestDestinationPacket.path = _pathStepper.CurrentPath;
                _destinationRequest = false;
            }   
            
            _latestStepPacket.info.serverTick = tick;
            _latestStepPacket.info.serverTimestamp = NetworkTime.localTime;

            _latestStepPacket.position = _pathStepper.DoStep(_stepSize);
            
            if (_destinationRequest)
                SendToClient(_latestDestinationPacket);
            SendToClient(_latestStepPacket);
        }
        
        // Transfer methods
        
        [Client]
        private void SendToServer(StepPacket pPacket)
        {
            CmdStep(pPacket);
        }
        
        [Client]
        private void SendToServer(DestinationPacket pPacket)
        {
            CmdDestination(pPacket);
        }

        [Server]
        private void SendToClient(StepPacket pPacket)
        {
            RpcStep(pPacket);
        }
        
        [Server]
        private void SendToClient(DestinationPacket pPacket)
        {
            RpcDestination(pPacket);
        }

        [Command(channel = Channels.Reliable)]
        private void CmdStep(StepPacket pPacket)
        {
            _latestStepPacket = pPacket;
        }
        
        [Command(channel = Channels.Reliable)]
        private void CmdDestination(DestinationPacket pPacket)
        {
            _destinationRequest = true;
            _latestDestinationPacket = pPacket;
        }

        [ClientRpc(channel = Channels.Reliable)]
        private void RpcStep(StepPacket pPacket)
        {
            ClientHandlePacket(pPacket);
        }
        
        [ClientRpc(channel = Channels.Reliable)]
        private void RpcDestination(DestinationPacket pPacket)
        {
            ClientHandlePacket(pPacket);
        }
    }
}
