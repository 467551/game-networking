using Mirror;
using Network_Packets;
using Player.NetworkHandlers;
using UnityEngine;

namespace Player
{
    public class PlayerDestination : NetworkComponent
    {
        [SerializeField] private PathStepper pathStepper;

        private bool destinationRequest = false;
        private Vector3 latestDestination;
        private DestinationPacket latestPacket;

        // Client
        public override void ClientStart()
        {
            
        }

        public override void ClientTick(uint tick)
        {
            if (destinationRequest)
            {
                pathStepper.SetDestination(latestDestination);
                PacketInfo info = new PacketInfo() {clientTick = tick,clientTimestamp = NetworkTime.localTime};
                SendToServer(latestDestination,info);
            }
        }

        // Called from the player controller
        public void UpdateDestination(Vector3 position)
        {
            destinationRequest = true;
            latestDestination = position;
        }
        
        // Server
        public override void ServerStart()
        {
            
        }

        public override void ServerTick(uint tick)
        {
            if (destinationRequest)
            {
                pathStepper.SetDestination(latestPacket.destination);
                latestPacket.path = pathStepper.CurrentPath;
                SendToClient(latestPacket);
                destinationRequest = false;
            }
        }

        // Transfer methods
        
        [Client]
        private void SendToServer(Vector3 pDestination, PacketInfo pInfo)
        {
            DestinationPacket packet = new DestinationPacket(){destination= pDestination, info = pInfo };
            CmdDestination(packet);
        }

        [Server]
        private void SendToClient(DestinationPacket packet)
        {
            RpcDestination(packet);
        }

        [Command(channel = Channels.Reliable)]
        private void CmdDestination(DestinationPacket packet)
        {
            destinationRequest = true;
            latestPacket = packet;
        }

        [ClientRpc(channel = Channels.Reliable)]
        private void RpcDestination(DestinationPacket packet)
        {
            destinationRequest = true;
            latestPacket = packet;
        }
    }
}
