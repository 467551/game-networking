using Mirror;
using Network_Packets;
using Player.NetworkHandlers;
using UnityEngine;

namespace Player.NetworkComponents
{
    public class PlayerInfo : NetworkComponent
    {
        private PlayerInfoPacket latestPlayerinfo;
        private bool newPlayerInfo = false;
        public new string name {get => latestPlayerinfo.playerName;}
        
        public override void ClientStart()
        {
            string requestedPlayername  = PlayerPrefs.GetString("PlayerName");
            PlayerInfoPacket packet = new PlayerInfoPacket() {playerName = requestedPlayername};
            SendToServer(packet);
        }

        private void HandlePacket(PlayerInfoPacket packet)
        {
            // update the view from this players object
            
            parentView.SetName(packet.playerName);
            PlayerPrefs.SetString("PlayerName",packet.playerName);
        }

        public override void ServerTick(uint tick)
        {
            if (newPlayerInfo)
            {
                SendToClient(latestPlayerinfo);
                newPlayerInfo = false;                
            }
        }
        
        // Transfer methods
        [Client]
        private void SendToServer(PlayerInfoPacket packet)
        {
            CmdPlayerInfo(packet);
        }

        [Server]
        private void SendToClient(PlayerInfoPacket packet)
        {
            RpcPlayerInfo(packet);
        }

        [Command(channel = Channels.Reliable)]
        private void CmdPlayerInfo(PlayerInfoPacket packet)
        {
            latestPlayerinfo = packet;
            newPlayerInfo = true;

            foreach (var keyPairConnection in NetworkServer.connections)
                keyPairConnection.Value.identity.gameObject.GetComponent<PlayerInfo>().UpdatePlayerInfo();
        }

        [ClientRpc(channel = Channels.Reliable)]
        private void RpcPlayerInfo(PlayerInfoPacket packet)
        {
            HandlePacket(packet);
        }

        [Server]
        public void UpdatePlayerInfo()
        {
            SendToClient(latestPlayerinfo);
        }
    }
}
