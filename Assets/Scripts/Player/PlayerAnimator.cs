using System;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Animator))]
    public class PlayerAnimator : MonoBehaviour
    {
        [SerializeField] private PlayerInterpolator playerInterpolator;

        [SerializeField] private float rotateSpeed = 1000f;
        private Animator animator;        
        
        private Vector3 velocity;
        private static readonly int Velocity = Animator.StringToHash("Velocity");

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            MoveAnimation();
            RotateModel();
        }

        private void RotateModel()
        {
            if (velocity.magnitude > 0)
            {
                var q = Quaternion.LookRotation(-velocity);
                var qrt = Quaternion.RotateTowards(transform.rotation, q, rotateSpeed * Time.deltaTime);
                transform.rotation = qrt;
            }
        }

        private void MoveAnimation()
        {
            velocity = playerInterpolator.Velocity;
            animator.SetFloat(Velocity,velocity.magnitude);
        }
    }
}
