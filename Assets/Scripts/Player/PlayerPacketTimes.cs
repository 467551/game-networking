using Mirror;
using Network_Packets;
using UnityEngine;

namespace Player
{
    public class PlayerPacketTimes
    {
        // EMA implementation for rtt
        // it calculates an exponential moving average roughly equivalent to the last n observations
        // https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
    
        private double _averageServerTimestampDelta; // Average time between server timestamps
        private double previousServerTimestampDelta;

        private double _avarageRtt;
        
        private float _emaAlpha = 2.0f / (10 + 1); // standard N-amount EMA alpha calculation where n == 10
        private bool _rttInit = false;
        private float _stepDelta = 1;
    
        public float StepDelta
        {
            get => _stepDelta;
        }

        public double AverageServerDelta
        {
            get => _averageServerTimestampDelta;
        }

        public double AvarageRTT
        {
            get => _avarageRtt;
        }

        public void CalculateTimestamps(PacketInfo pPacketInfo)
        {
            if (!_rttInit)
            {
                _avarageRtt = NetworkTime.localTime - pPacketInfo.clientTimestamp;
                
                _rttInit = true;
                previousServerTimestampDelta = pPacketInfo.serverTimestamp;
                _averageServerTimestampDelta = 1f / NetworkManager.singleton.serverTickRate;
            }
            else
            {
                // RTT 
                double deltaRTT = (NetworkTime.localTime - pPacketInfo.clientTimestamp) - _avarageRtt;
                _avarageRtt += _emaAlpha * deltaRTT;
                
                // Server timestamp difference
                double newST = pPacketInfo.serverTimestamp - previousServerTimestampDelta;
            
                double deltaST = newST - _averageServerTimestampDelta;
                _averageServerTimestampDelta += _emaAlpha * deltaST;
                previousServerTimestampDelta = pPacketInfo.serverTimestamp;
            
                _stepDelta = (float) (1f - (1f / NetworkManager.singleton.serverTickRate - _averageServerTimestampDelta));
            }
        }

        public void LogNetworkTimes()
        {
            Debug.Log("st : " + _averageServerTimestampDelta);
        }
    }
}
