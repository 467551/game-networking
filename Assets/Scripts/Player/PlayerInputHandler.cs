using Mirror;
using Player.NetworkComponents;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerInputHandler : MonoBehaviour
    {
        // side = 0 left, side = 1 right
        public delegate void MouseClick(InputAction.CallbackContext pCtx,Vector2 pScreenPos);
        public delegate void SkillPressedEvent(PlayerSkill playerSkill);
        public event MouseClick OnLeftMousePressed;
        public event MouseClick OnRightMousePressed;
        public event SkillPressedEvent OnSkilledPressed;

        public void LeftMouseClickEvent(InputAction.CallbackContext ctx)
        {
            OnLeftMousePressed?.Invoke(ctx,Mouse.current.position.ReadValue());
        }

        public void RightMouseClickEvent(InputAction.CallbackContext ctx)
        {
            OnRightMousePressed?.Invoke(ctx, Mouse.current.position.ReadValue());
        }
    }
}