using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Network_Packets;
using Player.NetworkComponents;
using Player.NetworkHandlers;
using UnityEngine;

namespace Player
{
    public class PlayerNetworkComponent : NetworkBehaviour
    {
        [Header("Input")]
        [SerializeField] private PlayerInputHandler _playerInput;
        
        [Header("Required NetworkComponents")]
        [SerializeField] private NetworkStatus _status;
        [SerializeField] private PlayerInfo _playerInfo;
        
        [Header("Optional NetworkComponents")]
        [SerializeField] private List<NetworkComponent> _networkComponents;
        
        [Header("Player view")]
        [SerializeField] private GameObject _playerViewPrefab;
        private PlayerView _playerView;
        [Header("Veriables")] [SerializeField] public PlayerVariables variables;
        
        private uint _currentTick = 0;
        
        public NetworkStatus status {get => _status;}
        public PlayerInfo playerInfo {get => _playerInfo;}

        public override void OnStartClient()
        {
            CreatePlayerView();
            
            _networkComponents.Insert(0,_status);
            _networkComponents.Insert(1,_playerInfo);

            foreach (var networkComponent in _networkComponents)
            {
                networkComponent.SetComponentParent(this);
                networkComponent.SetView(_playerView);
                networkComponent.SetInputHandler(_playerInput);
                
                networkComponent.ClientStart();
            }

            if (isLocalPlayer)
            {
                gameObject.tag = "LocalPlayer";
                StartCoroutine(nameof(LocalTick));
            }
            else
            {
                gameObject.tag = "Player";
            }
        }
        
        public override void OnStartServer()
        {
            _networkComponents.Insert(0,_status);
            _networkComponents.Insert(1,_playerInfo);

            foreach (var networkComponent in _networkComponents)
            {
                networkComponent.SetComponentParent(this);

                networkComponent.ServerStart();
            }
        }

        private void CreatePlayerView()
        {
            GameObject viewObject = Instantiate(_playerViewPrefab,transform.position,Quaternion.identity,transform.parent);
            _playerView = viewObject.GetComponent<PlayerView>();

            if (_playerViewPrefab == null || _playerView == null)
            {
                throw new Exception("Player view prefab not initialized or it did not have an player view component");
            }
            
            _playerView.Init(isLocalPlayer);
        }

        // We use the normal unity update for the server since the server will run on the frame rate based up on its tick rate
        // This will be move efficient since on the server we are not dependent on a 60+ fps gameplay
        public void Update()
        {
            if (isServer)
            {
                _currentTick++;
                foreach (var networkComponent in _networkComponents)
                    networkComponent.ServerTick(_currentTick);
            }
        }

        // We use an Ienumerator for the client since we want the update it gets from the server to be independent from this frame rate, so we can run on a lower tickrate
        private IEnumerator LocalTick()
        {
            while (true)
            {
                yield return new WaitForSeconds(1f / NetworkManager.singleton.serverTickRate);
                
                _currentTick++;

                foreach (var networkComponent in _networkComponents)
                    networkComponent.ClientTick(_currentTick);
            }
        }

        public override void OnStopClient()
        {
            Destroy(_playerView.gameObject);
        }
    }
}
