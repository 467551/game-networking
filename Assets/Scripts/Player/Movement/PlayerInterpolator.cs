using UnityEngine;

namespace Player
{
    public class PlayerInterpolator : MonoBehaviour
    {
        public InterpolateTarget InterpolateTarget { get => interpolateTarget; }
        private InterpolateTarget interpolateTarget;

        public Vector3 Velocity
        {
            get => velocity;
        }
        private Vector3 velocity;
    
        private void Update()
        {
            float maxDistanceDelta = Time.deltaTime * interpolateTarget.interpolateStepSize;
            var oldPosition = transform.position;
            Vector3 moveVector = Vector3.MoveTowards(oldPosition, interpolateTarget.target, maxDistanceDelta);
            velocity = (oldPosition - interpolateTarget.target).normalized * interpolateTarget.interpolateStepSize;
            transform.position = moveVector;
        }

        public void Set(Vector3 pos,double pInterpolationTime,float pStepSize)
        {
            InterpolateTarget target = new InterpolateTarget(pos, pInterpolationTime,pStepSize);
            interpolateTarget = target;
        }

        public void Set(InterpolateTarget target)
        {
            interpolateTarget = target;
        }
    }
    
    public struct InterpolateTarget
    {
        public Vector3 target;
        public double interpolateTime;
        public float interpolateStepSize;

        public InterpolateTarget(Vector3 pTarget, double pInterpolateTime, float pInterpolateStepSize)
        {
            target = pTarget;
            interpolateTime = pInterpolateTime;
            interpolateStepSize = pInterpolateStepSize;
        }
    }
}