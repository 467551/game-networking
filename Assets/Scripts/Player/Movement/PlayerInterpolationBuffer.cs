using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(PlayerInterpolator))]
    public class PlayerInterpolationBuffer : MonoBehaviour
    {
        private PlayerInterpolator interpolator;
        
        [Header("Veriables")]
        [SerializeField] [Range(1, 8)] private int interpolationBufferSize = 2;
        private bool interpolationRunning = false;

        private Queue<InterpolateTarget> interpolationBuffer;
        public int debugSize = 0;

        private void Awake()
        {
            interpolator = GetComponent<PlayerInterpolator>();
            interpolationBuffer = new Queue<InterpolateTarget>();
        }
        
        private void Update()
        {
            int bufferCount = interpolationBuffer.Count;
            debugSize = bufferCount;

            if (bufferCount > interpolationBufferSize && !interpolationRunning)
            {
                StartCoroutine(StartInterpolate());
            }
        }

        public void Add(Vector3 pos,double pInterpolationTime,float pStepSize)
        {
            InterpolateTarget target = new InterpolateTarget(pos, pInterpolationTime,pStepSize);
            interpolationBuffer.Enqueue(target);
        }

        public void Add(InterpolateTarget pTarget)
        {
            interpolationBuffer.Enqueue(pTarget);
        }
        
        private IEnumerator StartInterpolate()
        {
            interpolationRunning = true;
            while (interpolationRunning)
            {
                InterpolateTarget target = interpolationBuffer.Peek();
                interpolator.Set(target);
                //Debug.Log("time = " + (float)target.interpolateTime);
                yield return new WaitForSeconds((float)target.interpolateTime);
                
                //bool logWarning = false;
                while (interpolationBuffer.Count > interpolationBufferSize+1)
                {
                    // if (!logWarning)
                    // {
                    //     Debug.Log("emptying excessive buffer");
                    //     logWarning = true;
                    // }
                    interpolationBuffer.Dequeue();
                }
            
                if (interpolationBuffer.Count > 1)
                {
                    interpolationBuffer.Dequeue();
                }
                else if (interpolationBuffer.Count == 1)
                {
                    interpolationBuffer.Dequeue();
                    interpolationRunning = false;
                    //Debug.Log("buffer empty after last");
                    break;
                }
                else
                {
                    //Debug.Log("buffer empty unexpected");
                    interpolationRunning = false;
                    break;   
                }
            }
            yield return null;
        }
    }
}

