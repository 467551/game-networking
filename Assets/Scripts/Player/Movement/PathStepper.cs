using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;


// This class can be used to take steps along a path based up on this gameobject's transform and a path that is created using the Unity navmesh.
// It uses its own movement system to move based up on its current path.
public class PathStepper : MonoBehaviour
{
    private Vector3 destination;
    private List<Vector3> pathNodes = new List<Vector3>(); // a path consists of nodes. There is a node for each corner along the path. Path creation is based up on Unity navmesh.
    private int targetIndex = 0; // The index where we are currently heading towards
    private int currentIndex = 0; // The index where we are / where we moving away from
    private int maxIndex = 0; // The highest index in on our current path;

    public Vector3 Destination { get => destination; }
    public Vector3[] CurrentPath { get => pathNodes.ToArray(); }

    // Let the agent do a new step along its current path.
    // return its new position
    // It will return its current position if we have no current path or when we finished on our path
    public Vector3 DoStep(float stepSize)
    {
        if (targetIndex != currentIndex)
        {
            Vector3 fromPosition = transform.position;
            
            Vector3 difference = pathNodes[targetIndex] - fromPosition;
            Vector3 moveVector = difference.normalized * stepSize;
            
            Vector3 cornetVector = Vector3.zero;

            if (moveVector.sqrMagnitude > difference.sqrMagnitude)
            {
                moveVector = difference;
                
                targetIndex++;
                currentIndex++;

                if (targetIndex > maxIndex) // End of path reached
                {
                    targetIndex = maxIndex;
                    currentIndex = maxIndex;
                }
                else // Move 
                {
                    float leftOver = stepSize - moveVector.magnitude;
                    cornetVector = (pathNodes[targetIndex] - pathNodes[currentIndex]).normalized * leftOver;
                }
            }

            Vector3 movePos = transform.position + moveVector + cornetVector;
            
            if (Physics.Raycast(movePos + new Vector3(0,1f,0),-Vector3.up, out RaycastHit hit))
            {
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("CityBuilding") || hit.collider.gameObject.layer == LayerMask.NameToLayer("ClickableTerrain"))
                {
                    movePos.y = hit.point.y;
                }
            }
            
            transform.position = movePos;
            return movePos;
        }
        return transform.position;
    }
    
    // Same as DoStep() but we do not actually set this transform to the new position, and only return what would be our new position if we where to take a step
    public Vector3 SimulateStep(float stepSize)
    {
        if (targetIndex != currentIndex)
        {
            Vector3 fromPosition = transform.position;
            
            Vector3 difference = pathNodes[targetIndex] - fromPosition;
            Vector3 moveVector = difference.normalized * stepSize;
            
            Vector3 cornetVector = Vector3.zero;

            if (moveVector.sqrMagnitude > difference.sqrMagnitude)
            {
                moveVector = difference;
                
                targetIndex++;
                currentIndex++;

                if (targetIndex > maxIndex) // End of path reached
                {
                    targetIndex = maxIndex;
                    currentIndex = maxIndex;
                }
                else // Move 
                {
                    float leftOver = stepSize - moveVector.magnitude;
                    cornetVector = (pathNodes[targetIndex] - pathNodes[currentIndex]).normalized * leftOver;
                }
            }

            Vector3 movePos = transform.position + moveVector + cornetVector;
            return movePos;
        }

        Vector3 stillPos = transform.position;
        return stillPos;
    }
    
    // Set the position of the path stepper manually
    public void ManualPosition(Vector3 newPosition)
    {
        transform.position = newPosition;
    }
    
    // Do a manual step
    public void ManualStep(Vector3 pStep)
    {
        transform.position += pStep;
    }
    
    // Calculates a path and returns it as a list of Vector3
    private List<Vector3> CalculatePath(Vector3 dest)
    {
        NavMeshPath meshPath = new NavMeshPath();
        NavMesh.CalculatePath(transform.position, dest, NavMesh.AllAreas, meshPath);
        List<Vector3> path = meshPath.corners.ToList();
        return path;
    }

    // Sets the path for the agent
    // If andStart is enabled, it will also start moving along that path
    public List<Vector3> SetPath(List<Vector3> pPath)
    {
        if (pPath.Count > 1)
        {
            pathNodes = pPath;
            targetIndex = 1;
            currentIndex = 0;
            maxIndex = pathNodes.Count - 1;
            destination = pathNodes[maxIndex];
        }

        return pathNodes;
    }

    // Create a new path and set that to be the current path to follow
    public List<Vector3> SetDestination(Vector3 point)
    {
        List<Vector3> path = CalculatePath(point);
        return SetPath(path);
    }
}
