using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Network_Packets;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(PathStepper))]
    public class MovePredictor : MonoBehaviour
    {
        private PathStepper _stepper;
        private List<BufferedStep> _movementBuffer;

        private void Awake()
        {
            _stepper = GetComponent<PathStepper>();
            _movementBuffer = new List<BufferedStep>();
        }

        public Vector3 DoStep(float pStepSize,uint pTick)
        {
            Vector3 prediction = _stepper.DoStep(pStepSize);
            _movementBuffer.Add(new BufferedStep(pStepSize,pTick));
            return prediction;
        }

        public void CheckPredictedMovement(StepPacket pPosPacket)
        {
            // First we move to where te server saw us because we know this is a valid position
            _stepper.ManualPosition(pPosPacket.position);
            
            // Then we empty our buffer until that movement
            int currentBufferSize = _movementBuffer.Count;

            for (int i = 0; i < currentBufferSize; i++)
            {
                if (_movementBuffer[i].tick < pPosPacket.info.clientTick)
                {
                    //Debug.Log("found a movement buffer that is older than what we have in our packet");
                    _movementBuffer.RemoveAt(i);
                    i--;
                    currentBufferSize--;
                }
                else if (_movementBuffer[i].tick == pPosPacket.info.clientTick)
                {
                    //Debug.Log("found our packet, so we remove this as well and break the loop here");
                    _movementBuffer.RemoveAt(i);
                    break;
                }
                else if (_movementBuffer[i].tick > pPosPacket.info.clientTick)
                {
                    //Debug.Log("Something went wrong : " + i);
                }
            }

            // Now we do all the steps left in our prediction buffer
            for (int i = 0; i < _movementBuffer.Count; i++)
            {
                //Debug.Log("step added from buffer : " + i);
                _stepper.DoStep(_movementBuffer[i].stepSize);
            }
            
        }

        public struct BufferedStep
        {
            public float stepSize;
            public uint tick;

            public BufferedStep(float pStepsize,uint pTick)
            {
                stepSize = pStepsize;
                tick = pTick;   
            }
        }
    }
}
