using System;
using Network_Packets;
using TMPro;
using UnityEngine;

namespace Player
{
    public class PlayerView : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private PlayerInterpolator _interpolator;
        [SerializeField] private PlayerInterpolationBuffer _interpolationBuffer;
        [SerializeField] private TextMeshProUGUI _nameplate;
        [SerializeField] private GameObject _uiPrefab;
        [SerializeField] private RectTransform _worldSpaceCanvas;
        [NonSerialized] public PlayerUI ui;
        
        private bool _isLocalPlayer = false;

        public void Awake()
        {
            gameObject.SetActive(false);
        }

        public void Update()
        {
            Camera main = Camera.main;
            _worldSpaceCanvas.transform.LookAt(transform.position + main.transform.rotation * Vector3.forward,transform.rotation * Vector3.up);
        }
        
        public void Init(bool pIsLocalPlayer)
        {
            gameObject.SetActive(true);
            _isLocalPlayer = pIsLocalPlayer;
            
            GameObject hudObj = GameObject.FindGameObjectWithTag("HUD");

            if (hudObj != null)
            {
                ui = hudObj.GetComponent<PlayerUI>();
            }
        }

        public void Move(InterpolateTarget pTarget)
        {
            if (_isLocalPlayer)
            {
                _interpolator.Set(pTarget);
            }
            else
            {
                //_interpolator.Set(pTarget);
                _interpolationBuffer.Add(pTarget);
            }
        }

        public void Move(Vector3 pTarget, double pInterpolateTime, float pInterpolateStepSize)
        {
            if (_isLocalPlayer)
            {
                _interpolator.Set(new InterpolateTarget(pTarget,pInterpolateTime,pInterpolateStepSize));
            }
            else
            {
                //_interpolator.Set(new InterpolateTarget(pTarget,pInterpolateTime,pInterpolateStepSize));
                _interpolationBuffer.Add(new InterpolateTarget(pTarget,pInterpolateTime,pInterpolateStepSize));
            }
        }
        
        public void SetChatMessage(string pUserName, string pMessage)
        {
            ui.chatBox.ShowMessage(pUserName, pMessage);
        }

        public void SetName(string pName)
        {
            _nameplate.text = pName;
        }
    }
}
