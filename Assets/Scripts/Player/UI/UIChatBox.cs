using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player.UI
{
    public class UIChatBox : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _inputField;
        [SerializeField] private RectTransform _viewportContent;
        [SerializeField] private GameObject _chatMessagePrefab;

        public delegate void SendChatMessageRequest(string message);
        public event SendChatMessageRequest OnSendChatMessage;

        public void ShowMessage(string pPlayerName, string pMessage)
        {
            UIChatMessage messageObj = Instantiate(_chatMessagePrefab).GetComponent<UIChatMessage>();
            messageObj.SetMessage(pPlayerName + ": " + pMessage);
            messageObj.transform.SetParent(_viewportContent);
        }

        public void OnSend()
        {
            if (_inputField.text != string.Empty && _inputField.text.Length > 64)
                return;
        
            OnSendChatMessage?.Invoke(_inputField.text);
            ClearInput();
        }

        public void OnEnterPressed(InputAction.CallbackContext ctx)
        {
            if (_inputField.text != string.Empty && _inputField.text.Length > 64)
                return;
        
            switch (ctx.phase)
            {
                case InputActionPhase.Performed:
                    OnSendChatMessage?.Invoke(_inputField.text);
                    ClearInput();
                    break;
            }
        }

        private void ClearInput()
        {
            _inputField.text = "";
        }
    }
}