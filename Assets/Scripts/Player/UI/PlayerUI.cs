using Mirror;
using Network_Packets;
using Player.UI;
using UnityEngine;

namespace Player
{
    public class PlayerUI : MonoBehaviour
    {
        public NetworkInfoUI networkInfoUI;
        public UIChatBox chatBox;
    }
}
