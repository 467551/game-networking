using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIChatMessage : MonoBehaviour
{
    [SerializeField] private float _timeTillHide = 10f;
    [SerializeField] private TextMeshProUGUI _message;

    private float _timeLeft = 0f;
    
    private void Update()
    {
        if (_timeLeft > 0f)
        {
            _timeLeft -= Time.deltaTime;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void SetMessage(string pMessage)
    {
        _message.text = pMessage;
        _timeLeft = _timeTillHide;
    }
}
