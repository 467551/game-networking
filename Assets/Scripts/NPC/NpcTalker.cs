using System;
using Mirror;
using Network_Packets;
using Player;
using UnityEngine;

namespace NPC
{
    [RequireComponent(typeof(Collider),typeof(Animator))]
    public class NpcTalker : NetworkBehaviour
    {
        [SerializeField] private string npcName;
        [SerializeField] private string message;

        private PlayerUI playerUI;
        private Animator animator;

        public override void OnStartClient()
        {
            animator = GetComponent<Animator>();
            
            GameObject hudObj = GameObject.FindGameObjectWithTag("HUD");

            if (hudObj != null)
            {
                playerUI = hudObj.GetComponent<PlayerUI>();
            }
        }

        [Client]
        private void HandlePacket(ChatMessagePacket pPacket)
        {
            animator.Play("Victory");
            playerUI.chatBox.ShowMessage(pPacket.playerName,pPacket.chatMessage);
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (!isServer)
                return;

            if (other.CompareTag("Player"))
            { 
                SendToClientTarget(other.gameObject.GetComponent<NetworkIdentity>());
            }
        }

        [Server]
        private void SendToClientTarget(NetworkIdentity identity)
        {
            ChatMessagePacket packet = new ChatMessagePacket() {playerName = npcName, chatMessage = message};
            RpcNpcMessage(identity.connectionToClient,packet);
        }
        
        [TargetRpc]
        private void RpcNpcMessage(NetworkConnection target,ChatMessagePacket pPacket)
        {
            HandlePacket(pPacket);
        }
    }
}
