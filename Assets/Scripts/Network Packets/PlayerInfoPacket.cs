using UnityEngine;

namespace Network_Packets
{
    [System.Serializable]
    public struct PlayerInfoPacket
    {
        public PacketInfo info;
        public string playerName;
    }
}
