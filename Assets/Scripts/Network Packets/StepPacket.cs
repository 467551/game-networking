using UnityEngine;
using UnityEngine.Serialization;

namespace Network_Packets
{
    [System.Serializable]
    public struct StepPacket
    { 
        public Vector3 position;
        public PacketInfo info;
    }
}
