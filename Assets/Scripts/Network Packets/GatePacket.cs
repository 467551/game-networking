using UnityEngine;

namespace Network_Packets
{
    [System.Serializable]
    public struct GatePacket
    {
        public bool openGate;
    }
}
