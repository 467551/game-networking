using UnityEngine;

namespace Network_Packets
{
    [System.Serializable]
    public struct DestinationPacket
    {
        public Vector3 destination;
        public Vector3[] path;
        public PacketInfo info;
    }
}
