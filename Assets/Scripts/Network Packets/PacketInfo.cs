namespace Network_Packets
{
    [System.Serializable]
    public struct PacketInfo  
    {
        public uint clientTick;
        public uint serverTick;

        public double clientTimestamp;
        public double serverTimestamp;
        public PacketInfo(uint pClientTick, uint pServerTick, double pClientTimestamp, double pServerTimestamp)
        {
            clientTick = pClientTick;
            serverTick = pServerTick;
            clientTimestamp = pClientTimestamp;
            serverTimestamp = pServerTimestamp;
        }
    }
}
