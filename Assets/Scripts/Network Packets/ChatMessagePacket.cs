using UnityEngine;

namespace Network_Packets
{
    public struct ChatMessagePacket
    {
        public PacketInfo info;
        public Vector3 positionMessenger;
        public string playerName;
        public string chatMessage;
    }
}
