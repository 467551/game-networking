using Player.NetworkHandlers;
using UnityEngine;

namespace Sound
{
    [RequireComponent(typeof(AudioSource))]
    public class ClientBackgroundMusic : NetworkComponent
    {
        [SerializeField] private AudioSource audioSource;
        
        public override void ClientStart()
        {
            if (isLocalPlayer)
                audioSource.Play();
        }
    }
}
