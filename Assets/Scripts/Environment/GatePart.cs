using System;
using System.Collections;
using UnityEngine;

namespace Environment
{
    public class GatePart : MonoBehaviour
    {
        public enum GateState
        {
            CLOSED = 0,
            OPEN,
            TRANSITION
        }

        private GateState _gateState = GateState.CLOSED;
        private GateState _targetState = GateState.CLOSED;
        
        [SerializeField] private Quaternion closedRotation, openRotation;

        private float _gateSpeed = 1f;
        private float _timeCount = 0f;

        private bool _hasSwingAnimation = false;

        public void OnValidate()
        {
            //closedRotation = transform.rotation;
        }

        public void UpdateGate(GateState gateState,bool pHasSwingAnimation)
        {
            _hasSwingAnimation = pHasSwingAnimation;
            if (gateState == GateState.OPEN) // open gate
            {
                _targetState = GateState.OPEN;
            }
            else // close gate
            {
                _targetState = GateState.CLOSED;
            }
        }

        public void Update()
        {
            if (_gateState != _targetState)
            {
                _gateState = GateState.TRANSITION;

                switch (_targetState)
                {
                    case GateState.OPEN:
                        if (!_hasSwingAnimation)
                            transform.rotation = openRotation;
                        else
                        {
                            transform.rotation = Quaternion.Lerp(transform.rotation,openRotation,Time.deltaTime * _gateSpeed);

                            if (transform.rotation.Equals(openRotation))
                                _gateState = GateState.OPEN;   
                        }

                        break;
                    case GateState.CLOSED:
                        if (!_hasSwingAnimation)
                            transform.rotation = closedRotation;
                        else
                        {
                            transform.rotation = Quaternion.Lerp(transform.rotation,closedRotation,Time.deltaTime * _gateSpeed);

                            if (transform.rotation.Equals(closedRotation))
                                _gateState = GateState.CLOSED;   
                        }
                        break;
                }
            }
        }
    }
}
