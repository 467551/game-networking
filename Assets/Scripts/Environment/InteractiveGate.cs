using System;
using System.Collections.Generic;
using Mirror;
using Network_Packets;
using UnityEngine;

namespace Environment
{
    public class InteractiveGate : NetworkBehaviour
    {
        [SerializeField] private List<GatePart> gateParts;
        
        [SerializeField] private PlayerTrigger ButtonA;
        [SerializeField] private PlayerTrigger ButtonB;

        [SerializeField] private PlayerTrigger ButtonZ;

        [SerializeField] private float _closeTime = 10f;
        private float _timeToClose = 0f;
        private bool _gateIsOpen = false;

        [Client]
        public void OnGatePacket(GatePacket pPacket)
        {
            foreach (var gate in gateParts)
            {
                if (pPacket.openGate)
                {
                    gate.UpdateGate(GatePart.GateState.OPEN,true);
                }
                else
                {
                    gate.UpdateGate(GatePart.GateState.CLOSED,true);
                }
            }
        }
        public override void OnStartServer()
        {
            ButtonA.OnPlayerEnter += OnButtonAEnter;
            ButtonB.OnPlayerEnter += OnButtonBEnter;
            ButtonZ.OnPlayerEnter += OpenGates;
        }

        private void Update()
        {
            if (isServer && _gateIsOpen)
            {
                _timeToClose -= Time.deltaTime;
                if (_timeToClose <= 0)
                    CloseGates();
                
            }
        }

        [Server]
        private void OnButtonAEnter()
        {
            if (ButtonB.isPlayerInTrigger)
            {
                OpenGates();
                // Open gate
            }
        }

        [Server]
        private void OnButtonBEnter()
        {
            if (ButtonA.isPlayerInTrigger)
            {
                // Open gate
                OpenGates();
            }
        }

        [Server]
        private void OpenGates()
        {
            _timeToClose = _closeTime;
            _gateIsOpen = true;
            foreach (var gate in gateParts)
            {
                gate.UpdateGate(GatePart.GateState.OPEN,false);
            }
            RpcGateUpdate(new GatePacket() {openGate = true});
        }

        [Server]
        private void CloseGates()
        {
            _gateIsOpen = false;
            foreach (var gate in gateParts)
            {
                gate.UpdateGate(GatePart.GateState.CLOSED,false);
            }
            RpcGateUpdate(new GatePacket() {openGate = false});
        }

        [ClientRpc]
        private void RpcGateUpdate(GatePacket pPacket)
        {
            OnGatePacket(pPacket);
        }
    }
}
