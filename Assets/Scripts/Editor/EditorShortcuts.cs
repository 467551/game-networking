using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorShortcuts : Editor
{
    [MenuItem("Tools/Toggle Lock Inspector _F10")]
    static void LockUnlockInspector()
    {
        if (ActiveEditorTracker.sharedTracker.activeEditors.Length > 0)
        {
            ActiveEditorTracker.sharedTracker.isLocked = !ActiveEditorTracker.sharedTracker.isLocked;
            ActiveEditorTracker.sharedTracker.activeEditors[0].Repaint();
        }
    }
}
