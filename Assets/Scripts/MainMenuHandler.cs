using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuHandler : MonoBehaviour
{
    [SerializeField]
    private ExtendedNetworkManager _networkManager = null;

    [SerializeField]
    private TMP_InputField _ipInputField = null,_playerName = null;
    
    private string[] _randomNames = { 
        "Mahesh Chand", "Jeff Prosise", "Dave McCarter", "Allen O'neill",  
        "Monica Rathbun", "Henry He", "Raj Kumar", "Mark Prime",  
        "Rose Tracey", "Mike Crown" };
    
    
    public void OnHostServer()
    {
        //networkManager.StartHost();
    }

    public void OnJoinLocalHost()
    {
        if (_playerName.text != string.Empty && _playerName.text.Length < 19)
        {
            PlayerPrefs.SetString("PlayerName",_playerName.text);
        }
        else
        {
            int randomIndex = Random.Range(0, _randomNames.Length);
            PlayerPrefs.SetString("PlayerName",_randomNames[randomIndex]);
        }
        
        _networkManager.networkAddress = "localhost";
        _networkManager.StartClient();
    }

    public void OnJoinIP()
    {
        if (_playerName.text != string.Empty && _playerName.text.Length < 19)
        {
            PlayerPrefs.SetString("PlayerName",_playerName.text);
        }
        else
        {
            int randomIndex = Random.Range(0, _randomNames.Length);
            PlayerPrefs.SetString("PlayerName",_randomNames[randomIndex]);
        }
        
        _networkManager.networkAddress = _ipInputField.text;
        _networkManager.StartClient();
    }
}
